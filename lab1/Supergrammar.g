grammar Supergrammar;

@members {
	class ZeroDivisionException extends RuntimeException {
		ZeroDivisionException() {
			System.out.println("Pamietaj, cholero, nie dziel przez 0.");
		}
	}
}

plik 
	:	line* EOF
	;
	
line 
	:	topexpr=bitorexpr {System.out.println($topexpr.value);} NL+
	;

bitorexpr returns [int value]
	:	e1=bitxorexpr {$value = $e1.value; } ((BITOR e2=bitxorexpr {$value |= $e2.value; }))*
	;

bitxorexpr returns [int value]
	:	e1=bitandexpr {$value = $e1.value; } ((BITXOR e2=bitandexpr {$value ^= $e2.value; }))*
	;
	
bitandexpr returns [int value]
	:	e1=shiftexpr {$value = $e1.value; } ((BITAND e2=shiftexpr {$value &= $e2.value; }))*
	;

shiftexpr returns [int value]
	:	e1=expr {$value = $e1.value; } ((LSHIFT e2=expr {$value <<= $e2.value; }
	| 	RSHIFT e2=expr {$value >>= $e2.value; }))*
	;	

expr returns [int value]
	:	t1=term {$value = $t1.value;} ((PLUS t2=term {$value += $t2.value;}
	|	MINUS t2=term {$value -= $t2.value;}))*
	;
	catch [ZeroDivisionException foo] {
		$value=-123123123;
		consumeUntil(input, NL); 
	}
	
term returns [int value]
	:	a1=atom {$value = $a1.value;} ((MUL a2=atom {$value *= $a2.value;}
	|	DIV a2=atom {
			if ($a2.value != 0) {
				$value /= $a2.value;
			}
			else {
				throw new ZeroDivisionException();
			}
		}
	|	MOD a2=atom {$value \%= $a2.value; }))*
	;

atom returns [int value]
	:	INT {$value = Integer.parseInt($INT.text);}
	|	(LP expr RP) {$value = $expr.value;}
	;
	
ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :	'0'..'9'+
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        ) {$channel=HIDDEN;}
    ;

MUL	:	'*'
	;

DIV	:	'/'
	;
	
MOD
	:	'%'
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;
	
LSHIFT
	:	'<<'
	;
	
RSHIFT
	:	'>>'
	;
	
BITAND 
	:	'&'
	;
	
BITXOR 
	:	'^'
	;

BITOR
	:	'|'
	;

LP	:	'('
	;

RP	:	')'
	;
	
NL 	:	'\n'
	;

